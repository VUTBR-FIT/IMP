/**
 * Autor: Lukas Cerny, xcerny63
 * Zmeny: 74%
 * Datum: 12.12.2016
 */

/*******************************************************************************
   main.c: LCD + keyboard demo
   Copyright (C) 2009 Brno University of Technology,
                      Faculty of Information Technology
   Author(s): Zdenek Vasicek <vasicek AT stud.fit.vutbr.cz>

   LICENSE TERMS

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:
   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in
      the documentation and/or other materials provided with the
      distribution.
   3. All advertising materials mentioning features or use of this software
      or firmware must display the following acknowledgement:

        This product includes software developed by the University of
        Technology, Faculty of Information Technology, Brno and its
        contributors.

   4. Neither the name of the Company nor the names of its contributors
      may be used to endorse or promote products derived from this
      software without specific prior written permission.

   This software or firmware is provided ``as is'', and any express or implied
   warranties, including, but not limited to, the implied warranties of
   merchantability and fitness for a particular purpose are disclaimed.
   In no event shall the company or contributors be liable for any
   direct, indirect, incidental, special, exemplary, or consequential
   damages (including, but not limited to, procurement of substitute
   goods or services; loss of use, data, or profits; or business
   interruption) however caused and on any theory of liability, whether
   in contract, strict liability, or tort (including negligence or
   otherwise) arising in any way out of the use of this software, even
   if advised of the possibility of such damage.

   $Id$


 *******************************************************************************/

#include <stdbool.h>

#include <fitkitlib.h>  
#include <keyboard/keyboard.h> 
#include <lcd/display.h>

// Pocet podlazi
#define SIZE 4

/**
 * Stavy výtahu
 */
enum {
    ST_INIT = 0,
    ST_INIT_01,
    ST_INIT_02,
    ST_STOP,
    ST_OPENING,
    ST_OPENING_01,
    ST_OPENED,
    ST_CLOSING,
    ST_ERROR,
    ST_CONTINUE,
    ST_WORK_UP,
    ST_WORK_DOWN
} LIFT_STATES;

/**
 * Stavy pohybu výtahu
 */
enum {
    DIR_DOWN = -1,
    DIR_STOP,
    DIR_UP
} LIFT_DIRECTION;

/** BIT  ID
 *   0  -1 - Sklep
 *   1   1 - 1. patro (Přízemí)
 *   2   2 - 2. patro
 *   3   3 - 3. patro
 */
unsigned char floor[SIZE];      // Podlazi kde zastavit
unsigned char led_floor[SIZE];  // Rozsvicene diody na podlazi
unsigned char led_lift[SIZE];   // Rozsvicene diody ve vytahu
int mode;       // 0(A)-Výtah 1(B)-Privolavac na patre
int direction;  // 1 -UP 0 -STOP -1 -DOWN
int lift_state;// Stav výtahu
int onFloor;   // Podlazi, ke kterumu se muze privolat vytah
int possition; // Podlazi, na kterem se nachazi vytah
char last_ch; //naposledy precteny znak

/**
 * Vypis uzivatelske napovedy (funkce se vola pri vykonavani prikazu "help")
 */
void print_user_help(void) {
    term_send_str("0-3- cislo patra");
    term_send_crlf();
    term_send_str("A  - rezim kabina vytahu");
    term_send_crlf();
    term_send_str("B  - rezim privolavaci panel");
    term_send_crlf();
    term_send_str("D  - oprava vytahu");
    term_send_crlf();
    term_send_str("*  - privolavaci tlacitko");
    term_send_crlf();
    term_send_str("#  - STOP");
}

/**
 * Inicializace periferii/komponent po naprogramovani FPGA
 */
void fpga_initialized() {
    LCD_init();
    LCD_clear();
    LCD_append_string("IMP - Vytah");
}

unsigned char decode_user_cmd(char *cmd_ucase, char *cmd) {
    return CMD_UNKNOWN;
}


unsigned char setBit(unsigned char array, int offset, int value);
void turnOnLED(int pos);
void activateLift(int pos);
void writeMode(bool terminal);

void turnOnLED(int pos) {
    if (pos < SIZE && pos >= 0) {
        floor[pos] = 1;
        if (mode == 0) {
            led_lift[pos] = 1;
        } else {
            led_floor[pos] = 1;
        }
    }
}

/**
 * Uvede nepohybujici vytah do pohybu
 * @param int   pos   Pozice patro, kde bylo zmacknuto tlacitko
 */
void activateLift(int pos) {
    if (direction == DIR_STOP) {
        if (pos == possition) {
            lift_state = ST_OPENING;
        } else if (pos < possition) {
            lift_state = ST_WORK_DOWN;
            direction = DIR_DOWN;
        } else if (pos > possition) {
            lift_state = ST_WORK_UP;
            direction = DIR_UP;
        }
    }
}

/**
 * Zjisti jestli exituje tlacitko zmacknute nad aktualni pozici
 * @return  bool  true-existuje false-neexistuje
 */
bool isUp() {
    int i;
    for (i = possition + 1; i < SIZE; i++) {
        if (floor[i] == 1) return (true);
    }
    return (false);
}

/**
 * Zjisti jestli exituje tlacitko zmacknute pod aktualni pozici
 * @return  bool  true-existuje false-neexistuje
 */
bool isDown() {
    int i;
    for (i = possition - 1; i >= 0; i--) {
        if (floor[i] == 1) return (true);
    }
    return (false);
}

/**
 * Zjisti jestli na aktualni pozici je zmacknute pozici
 * @return  bool  true-zmacknute false-nezmacknute
 */
bool isPushed() {
    if (possition >= 0 && possition < SIZE) {
        return (floor[possition] == 1);
    }
    return (false);
}

/**
 * Vypise aktualni rezim na LCD ci terminal
 * @param bool  terminal  true-terminal false-LCD
 */
void writeMode(bool terminal) {
    if (terminal == false) {
        LCD_clear();
        if (mode == 1) {
            LCD_append_string("Na podlazi");
        } else {
            LCD_append_string("V kabine");
        }
    } else {
        if (mode == 1) {
            term_send_str("Rezim: Na podlazi (zadej podlazi)");
            term_send_crlf();
        } else {
            term_send_str("Rezim: V kabine");
            term_send_crlf();
        }
    }
}

/**
 * Vypise aktualni stav na LCD ci terminal
 * @param bool  terminal  true-terminal false-LCD
 */
void writeLiftState(bool terminal) {
    if (terminal == false) {
        LCD_clear();
        if (mode == 1) {
            LCD_append_string("Podl:");
            if (onFloor == 0) LCD_append_string("-1");
            else LCD_append_char(onFloor + '0');
            LCD_append_string(" ");
        }
        LCD_append_string("Vyt:");
        if (possition == 0) LCD_append_string("-1");
        else LCD_append_char(possition + '0');
        if (lift_state == ST_ERROR) {
            LCD_append_string("#");
        }
    } else {
        if (mode == 1) {
            term_send_str(">Podlazi:");
            if (onFloor == 0) term_send_str("-1");
            else term_send_char(onFloor + '0');
            term_send_str(" ");
        } else {
            term_send_str(">");
        }
        term_send_str("Vytah:");
        if (possition == 0) term_send_str("-1");
        else term_send_char(possition + '0');
        if (lift_state == ST_ERROR) {
            term_send_str(" STOP");
        }
        term_send_crlf();
    }
}

/**
 * Obsluha klavesnice
 */
int keyboard_idle() {
    char ch;
    ch = key_decode(read_word_keyboard_4x4());
    if (ch != last_ch) {
        last_ch = ch;
        if (ch != 0) {
            switch (ch) {
                case 'A':
                    mode = 0;
                    writeMode(true);
                    break;
                case 'B':
                    mode = 1;
                    writeMode(true);
                    break;
                case 'D':
                    if (mode == 0 && lift_state == ST_ERROR) {
                        int i;
                        for (i = 0; i < SIZE; i++) {
                            floor[i] = 0;
                            led_lift[i] = 0;
                            led_floor[i] = 0;
                        }
                        direction = DIR_STOP;
                        turnOnLED(1);
                        activateLift(1);
                        lift_state = ST_CONTINUE;
                        term_send_str("Vytah opraven");
                        term_send_crlf();
                    }
                    break;
                case '*':
                    if (mode == 1) {
                        term_send_str("Vytah privolan na podlazi ");
                        term_send_char(onFloor + '0');
                        term_send_crlf();
                        turnOnLED(onFloor);
                        activateLift(onFloor);
                    }
                    break;
                case '#':
                    if (mode == 0) {
                        lift_state = ST_ERROR;
                        term_send_str("Vytah zastaven u ");
                        term_send_char(possition + '0');
                        term_send_str(" podlazi");
                        term_send_crlf();
                    }
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                    if (mode == 0) {
                        turnOnLED(ch - '0');
                        activateLift(ch - '0');
                    } else {
                        onFloor = ch - '0';
                        term_send_str(">Jsem v podlazi ");
                        if (onFloor == 0) term_send_str("-1");
                        else term_send_char(ch);
                        term_send_crlf();
                    }
                    break;
            }
        }
    }
    return 0;
}

/*******************************************************************************
 * Hlavni funkce
 *******************************************************************************/
int main(void) {
    mode = 1;
    direction = DIR_STOP;
    lift_state = ST_INIT;
    possition = 1;
    onFloor = 1;
    int lastState = lift_state;
    int i;
    for (i = 0; i < SIZE; i++) {
        floor[i] = 0;
        led_lift[i] = 0;
        led_floor[i] = 0;
    }

    unsigned int cnt5 = 0;
    unsigned int cnt6 = 0;
    unsigned int cnt = 0;

    last_ch = 0;

    initialize_hardware();
    keyboard_init();

    // Signalaizace stojiciho vytahu
    set_led_d6(1); // rozsviceni D6
    set_led_d5(0); // rozsviceni D5

    while (1) {
        delay_ms(10);
        cnt++;
        cnt5++;
        if (cnt5 > 25) {
            cnt5 = 0;
            flip_led_d5();
        }
        if (direction == 0) {
            set_led_d6(1);
        } else {
            cnt6++;
            if (cnt6 > 50) {
                writeLiftState(false);
                cnt6 = 0;
                flip_led_d6();
            }
        }

        keyboard_idle(); // obsluha klavesnice
        terminal_idle(); // obsluha terminalu

        switch (lift_state) {
            case ST_INIT:
                if (cnt > 75) {
                    cnt = 0;
                    lift_state = ST_INIT_01;
                    writeMode(false);
                }
                break;
            case ST_INIT_01:
                if (cnt > 100) {
                    cnt = 0;
                    lift_state = ST_STOP;
                }
                break;
            case ST_STOP:
                if (cnt > 75) {
                    cnt = 0;
                    writeLiftState(false);
                }
                break;
            case ST_ERROR:
                cnt = 0;
                break;
            case ST_CONTINUE:
                cnt = 0;
                lift_state = lastState;
                if (lift_state == ST_STOP) activateLift(1);
                break;
            case ST_OPENING:
                if (cnt > 100) {
                    cnt = 0;
                    writeLiftState(true);
                    floor[possition] = 0;
                    led_floor[possition] = 0;
                    led_lift[possition] = 0;
                    lift_state = ST_OPENING_01;
                }
                break;
            case ST_OPENING_01:
                if (cnt > 100) {
                    cnt = 0;
                    term_send_str("Opening...");
                    term_send_crlf();
                    lift_state = ST_OPENED;
                }
                break;
            case ST_OPENED:
                if (cnt > 150) {
                    cnt = 0;
                    term_send_str("Opened");
                    term_send_crlf();
                    lift_state = ST_CLOSING;
                }
                break;
            case ST_CLOSING:
                if (cnt > 100) {
                    cnt = 0;
                    term_send_str("Closing...");
                    term_send_crlf();
                    if (direction == DIR_UP) {
                        if (isUp()) lift_state = ST_WORK_UP;
                        else {
                            if (isDown()) {
                                direction = DIR_DOWN;
                                lift_state = ST_WORK_DOWN;
                            } else {
                                direction = DIR_STOP;
                                lift_state = ST_STOP;
                            }
                        }
                    } else if (direction == DIR_DOWN) {
                        if (isDown()) lift_state = ST_WORK_DOWN;
                        else {
                            if (isUp()) {
                                direction = DIR_UP;
                                lift_state = ST_WORK_UP;
                            } else {
                                direction = DIR_STOP;
                                lift_state = ST_STOP;
                            }
                        }
                    } else {
                        floor[possition] = 0;
                        led_floor[possition] = 0;
                        led_lift[possition] = 0;
                        lift_state = ST_STOP;
                    }
                }
                break;
            case ST_WORK_UP:
                if (cnt > 100) {
                    writeLiftState(true);
                    cnt = 0;
                    possition++;
                    if (isPushed()) lift_state = ST_OPENING;
                }
                break;
            case ST_WORK_DOWN:
                if (cnt > 100) {
                    writeLiftState(true);
                    cnt = 0;
                    possition--;
                    if (isPushed()) lift_state = ST_OPENING;
                }
                break;

        }
        if (lift_state != ST_ERROR && lift_state != ST_CONTINUE) {
            lastState = lift_state;
        }
    }
}
